const httpRequest = (method, url, body, callback) => {
  const request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState === XMLHttpRequest.DONE) {
      if (this.status === 200) {
        const object = JSON.parse(this.responseText);
        callback(object);
      } else {
        console.log(this.status, this.statusText);
      }
    }
  };
  request.open(method, url, true);
  request.send(body);
};
export const httpService = {
  get: (url, callback) => httpRequest('GET', url, null, callback),
  post: (url, body, callback) => httpRequest('POST', url, body, callback)
};
