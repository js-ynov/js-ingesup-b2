const students = ['nicolas', 'romain', 'pierre'];
const goodStudent = students[0];
const badStudent = students[2];

const findGoodStudent = () => goodStudent;

export const studentService = {
  goodStudent,
  badStudent,
  findGoodStudent
};
