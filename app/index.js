import {httpService} from './common/http.service';
import {Student} from './student/student.model';

const ulElement = document.querySelector('ul');
httpService.get('students.json', students => {
  students
    .map(student => new Student(student.nom, student.note))
    .filter(student => student.mark > 10)
    .map(student => student.name + ' : ' + student.mark)
    .map(studentLabel => {
      const element = document.createElement('li');
      element.innerHTML = studentLabel;
      return element;
    })
    .forEach(element => {
      ulElement.appendChild(element);
    });
});

const button = document.querySelector('button');
button.addEventListener('click', () => {
  import(/* webpackChunkName: "student.service" */ './student/student.service').then((module) => {
    const student = module.studentService.findGoodStudent();
    console.log(student);
  }).catch(error => 'An error occurred while loading the component');
});

