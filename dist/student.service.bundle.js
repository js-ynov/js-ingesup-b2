(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],[
/* 0 */,
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "studentService", function() { return studentService; });
const students = ['nicolas', 'romain', 'pierre'];
const goodStudent = students[0];
const badStudent = students[2];

const findGoodStudent = () => goodStudent;

const studentService = {
  goodStudent,
  badStudent,
  findGoodStudent
};


/***/ })
]]);