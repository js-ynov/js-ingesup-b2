const path = require('path');

module.exports = {
  mode: 'production',
  entry: './app/index.js',
  optimization: {
    minimize: false
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};
